# generator-lambda-basic [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> This generator creates an absolutely minimal AWS Lambda app skeleton in typescript.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-lambda-basic using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo typescript
npm install -g generator-lambda-basic
```

Then generate your new project:

```bash
yo lambda-basic
```

## Build

```bash
npm run build
```


## Deployment

Zip all transpiled javascript files under the `js` folder
along with `node_modules`, and upload it to Lambda.

In the AWS Lambda console,
set the handler to `index.default`.



## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Harry Y](https://gitlab.com/realharry)


[npm-image]: https://badge.fury.io/js/generator-lambda-basic.svg
[npm-url]: https://npmjs.org/package/generator-lambda-basic
[travis-image]: https://travis-ci.org/harrywye/generator-lambda-basic.svg?branch=master
[travis-url]: https://travis-ci.org/harrywye/generator-lambda-basic
[daviddm-image]: https://david-dm.org/harrywye/generator-lambda-basic.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/harrywye/generator-lambda-basic
