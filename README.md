﻿# generator-lambda-basic
_Yeoman Generator, lambda-basic, for an AWS Lambda function in Typescript._


This generator creates an absolutely minimal Lambda Function app skeleton (in typescript).



## Installation

First, install [Yeoman](http://yeoman.io) and [generator-lambda-basic](https://www.npmjs.com/package/generator-lambda-basic) using [npm](https://www.npmjs.com/).
(You'll obviously need [node.js](https://nodejs.org/) installed on your system. Bower is optional.).

```bash
npm install -g yo typescript
npm install -g generator-lambda-basic
```

Then generate your new project:

```bash
mkdir my-lambda-project
cd my-lambda-project
yo lambda-basic
```


## Build

```bash
cd my-lambda-project
npm run build
```


## Deployment

You deploy all transpiled javascript files under the `js` folder
along with `node_modules`.

After uploading the code, 
set the handler to `index.default`
in the AWS Lambda console.


## License

MIT © [Harry Y](https://gitlab.com/yofactory)



